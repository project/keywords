<?php
/**
 * @file keywords_settings_history.class.inc
 * Contain class Settings_History.
 * @class Settings_History
 * Class that handle the history of changes of module settings.
 *
 *
 * Usage:
 *   include_once('keywords_settings_history.class.inc');
 *   $history = new Settings_History();
 *   if (!$history->is_current_settings_up_to_date())    $history->save_settings();
 */

Class Settings_History {


  /**
  * Формирует MD5-сумму сериализованного массива настроек модуля
  *
  * @return
  * string  MD5-сумма сериализованного массива настроек модуля
  */
  function md5_current_settings() {
    //Получить параметры, которые влияют на генерацию ключевиков:
    $v[] = variable_get('keywords_area', array('title' => 'title', 'teaser' => 'teaser'));
    $v[] = variable_get('keywords_max_words', '2');
    $v[] = variable_get('keywords_min_density', '2');
    $v[] = variable_get('keywords_stop_words', array('ru' => 'ru', 'en' => 'en'));
    return md5(serialize($v));
  }


  /**
  * Ключевики были сгенерированы в соответствии с актуальными настройками или нет
  *
  * @param $nid
  * integer   Node's ID
  * @return
  * boolean    TRUE - keywords are valid. FALSE - if keywords is not up-to-date.
  */
  function is_node_keywords_up_to_date($changed='') {
    /*
      if(!$nid)  return;
      //Получить дату ноды
      $sql = "SELECT changed FROM {node} WHERE nid=%d LIMIT 1";
      $date = db_fetch_array(db_query($sql, $nid));
    */
    if (!$changed)   return;
    //Получить из базы последнюю запись:
    $last_change = $this->get_last_settings_change();
    return (( ($changed - $last_change['changed']) > 0) ? TRUE : FALSE);
  }




  /**
  * Получает данные о последнем изменении настроек модуля
  *
  * @return
  * boolean    TRUE - if setting was not changed. FALSE - if settings is not up-to-date.
  */
  function is_current_settings_up_to_date() {
    //Получить md5 текущих настроек:
    $current = $this->md5_current_settings();
    //Получить из базы последнюю запись:
    $last_change = $this->get_last_settings_change();
    return (($last_change['md5']==$current) ? TRUE : FALSE);
  }



  /**
  * Получает данные о последнем изменении настроек модуля
  *
  * @return
  * array    Array with keys: 'date' and 'md5'
  */
  function get_last_settings_change() {
    $sql = "SELECT * FROM {keywords_settings_hystory} ORDER BY changed DESC LIMIT 1";
    return db_fetch_array(db_query($sql));
  }



  /**
  * Сохранить MD5-сумму сериализованного массива в истории
  *
  * @return
  * nothing
  */
  function save_settings() {
    //Сохранить в базе
    $sql ="INSERT INTO {keywords_settings_hystory} (`changed`, `md5`) VALUES (%d, '%s')";
    db_query($sql, time(), $this->md5_current_settings());
  }


} //Class end