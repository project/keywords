<?php
/**
 * @file keywords.pages.inc Module pages.
 */


/**
 * Page "Node Statistics"
 * Статистика нод
 */
function page_node_statistics($object) {
  $header = array(t('Status'), t('Count'), t('Has Keywords'));
  $count_published   = count_nodes(1);
  $count_unpublished = count_nodes(0);
  $count_parsed_published   = count_parsed_nodes(1);
  $count_parsed_unpublished = count_parsed_nodes(0);

  $rows[] = array(t('Published'), $count_published, $count_parsed_published);
  $rows[] = array(t('Unpublished'), $count_unpublished, $count_parsed_unpublished);
  $rows[] = array(t('All'), $count_published + $count_unpublished,
                      $count_parsed_published + $count_parsed_unpublished);

  return theme_table($header, $rows);
}




/**
 * Page "Site's Keywords"
 */
function page_all_keywords($object) {
  //Получаем общее количество ключевых слов по сайту:
  $sql = "SELECT SUM( density ) AS count FROM {keywords_nodes}";
  $result = db_fetch_array(db_query($sql));
  $total_keywords_count = $result['count'];


  $header = array(
    t('#'),
    array('data' => t('Keyword'), 'field' => 'k.text'),
    array('data' => t('Density'), 'field' => 'density', 'sort' => 'desc'),
//    array('data' => t('Words'), 'field' => 'k.phrase_type'),
    t('Nodes')
  );

  $rows = array();
  $sql = "SELECT k.text, SUM(kn.density) AS density, k.phrase_type, k.kwid
          FROM {keywords} as k
            LEFT JOIN {keywords_nodes} AS kn USING (kwid)
            LEFT JOIN {node} AS n USING (nid)
          WHERE n.status=1
          GROUP BY k.text";
  $sql .= tablesort_sql($header);
  $result = db_query($sql, $i);
  $i=1; //Keywords counter;
  while ($r = db_fetch_array($result)) {
    //TODO: Выдать как ссылку на страницу тега, если теги были заданы:
    //Получить список всех нод с этим ключевым словом:
    $nodes = db_query("SELECT kn.nid FROM {keywords_nodes} AS kn WHERE kn.kwid=%d", $r['kwid']);
    unset($node_list);
    while ($node = db_fetch_array($nodes)) {
      $node_list[] = l($node['nid'], 'node/'. $node['nid'] .'/keywords');
    }
    $rows[] = array(
        $i,
        $r['text'],
        $r['density'] .' ('. number_format(($r['density']/$total_keywords_count)*100, 1, ',', ' ') .'%)',
        /*$r['phrase_type'],*/
        implode(', ', $node_list)
    );
    $i++;
  }
  if (count($rows)>0) {
    $out .= theme_table($header, $rows);
  }


  if (!$out) {
    why_keywords_not_found();
    $out = '<p>'. t('Keywords is not found.') .'</p>';
  }
  return $out;
}









/**
 * Tab "Keywords" for node
 */
function page_node_keywords($object) {
  //Проверяем устарели ли ключевики:
  include_once('keywords_settings_history.class.inc');
  $history = new Settings_History();
  if (!$history->is_node_keywords_up_to_date($object->changed)) {
    //Ключевики устарели - пересчитать:
    generate_keywords_for_node($object->nid);
  }

  //Плотность ключевых слов ноды по всему сайту:
  $sql = "SELECT k.kwid, SUM(kn.density) AS site_density
          FROM {keywords} as k
            LEFT JOIN {keywords_nodes} AS kn USING (kwid)
          GROUP BY k.text";
  $result = db_query($sql);
  //Плотность по всему сайту.
  while ($r = db_fetch_array($result))   $site_density[$r['kwid']] = $r['site_density'];

  return theme_keywords_table($object->nid, $site_density);
}







/**
 * Формирует таблицу ключевых слов для ноды
 */
function theme_keywords_table($nid='', $site_density=array()) {
  if (!$nid)   return;

  //Подключаем статистику нод:
  include_once('keywords_statistics.class.inc');
  $statistics = new Keywords_Statistics();
  //Получаем общее количество слов:
  $total = $statistics->get_node_total_words($nid);
  if (!$total) {
    //Нет общего числа слов - значить нужно генерировать ключевики.
    //Вызвать пересохранение этой ноды, что сгенерировать ключевики:
    generate_keywords_for_node($nid);
    $total = $statistics->get_node_total_words($nid);
  }

  //Шапка таблицы:
  $header = array(
      t('#'),
      array('data' => t('Keyword'), 'field' => 'k.text', 'id' => 'text'),
//      array('data' => t('Words'), 'field' => 'k.phrase_type', 'id' => 'phrase_type'),
      array('data' => t('Title Density'), 'field' => 'kn.title', 'id' => 'title_density'),
      array('data' => t('Teaser Density'), 'field' => 'kn.teaser', 'id' => 'teaser_density'),
      array('data' => t('Body Density'), 'field' => 'kn.body', 'id' => 'body_density'),
      array('data' => t('Total Density'),
            'field' => 'kn.density', 'id' => 'total_density', 'sort' => 'desc'),
      t('Site\'s Density'),
  );
  $rows = array();

  $sql = "SELECT k.kwid, k.text, k.phrase_type, kn.density, kn.title, kn.teaser, kn.body
          FROM {keywords} as k
            LEFT JOIN {keywords_nodes} AS kn USING (kwid)
          WHERE kn.nid='%d'";

  $sql .= tablesort_sql($header);
  $result = db_query($sql, array($nid));
  $i=1; //Keywords counter;
  while ($r = db_fetch_array($result)) {
    $global_density = ((!isset($site_density[$r['kwid']])) ? $r['density'] : $site_density[$r['kwid']]);
    $rows[] = array(
        $i,
        $r['text'],
//        $r['phrase_type'],
        $r['title'],
        $r['teaser'],
        $r['body'],
        $r['density'] .' ('. number_format(($r['density']/$total)*100, 1, ',', ' ') .'%)',
        $global_density .' ('. number_format(($r['density']/$global_density)*100, 1, ',', ' ') .'%)',
    );
    $i++;
  }
  return ((count($rows)>0) ? theme_table($header, $rows) : FALSE);
}





/**
 * Tab "Text Statistics" for node
 */
function page_node_text_statistics($object) {
  //Подключаем статистику нод:
  include_once('keywords_statistics.class.inc');
  $statistics = new Keywords_Statistics();
  if (!$statistics->get_node_total_words($object->nid))    generate_keywords_for_node($object->nid);
  //Шапка таблицы:
  $header = array(t('Parameter'), t('Value'));
  $rows = $statistics->get_all($object->nid);
  return ((count($rows)>0) ? theme_table($header, $rows) : FALSE);
}




/**
 * Tab "Site's Names"
 */
function page_site_names($object) {
  global $_SERVER;
  $url = $_SERVER['HOST']; //'DrupalCookBook.RU'
  $short_title = variable_get('site_name', '');
  $keywords_found = FALSE;


  //Получить самое частое ключевое слово:
  $super_tag = 'Drupal';

  //start_words:
  $S = array(
      $url .': ',
      $short_title .'! ',
      'Не проходите мимо! - ',
  );
  //clue_words
  $C = array(
      ' и ',
      ' или ',
      ', ',
      ', а также ',
      ' - ',
  );

  //Endings:
  $E = array(
      ' - все о '. $super_tag .'!',
      ' - ищите у нас!',
      ' - на '. $url .'!',
  );



  //Получаем ключевые слова:
  $sql = "SELECT k.phrase_type, k.text, SUM(kn.density) AS density
          FROM {keywords} as k
            LEFT JOIN {keywords_nodes} AS kn USING (kwid)
            LEFT JOIN {node} AS n USING (nid)
          WHERE n.status=1
          GROUP BY k.text";
  $result = db_query($sql);
  while ($r = db_fetch_array($result)) {
    $keywords[] = array(
                'phrase_type' => $r['phrase_type'],
                'text'        => $r['text'],
                'density'     => $r['density']
    );
  }

  if (count($keywords)>0) {
     //Ключевые слова есть
    for ($i=0;$i<10;$i++) {
      $keys = array_rand($keywords, 2);
      $keywords_list = $keywords[$keys[0]]['text'] .' ('. $keywords[$keys[0]]['density'] .'),<br> '. $keywords[$keys[1]]['text'] .' ('. $keywords[$keys[1]]['density'] .')';

      if (rand(0, 1)) {
        //KCKE
        $site_name =  $keywords[$keys[0]]['text'] . $C[array_rand($C)] .
                      $keywords[$keys[1]]['text'] . $E[array_rand($E)];
      }
      else {
        //SKCK
        $site_name =  $S[array_rand($S)] . $keywords[$keys[0]]['text'] .
                      $C[array_rand($C)] . $keywords[$keys[1]]['text'];
      }
      $rows[] = array(mb_ucfirst($site_name, 'utf-8'), strlen($site_name), $keywords_list,
                      $keywords[$keys[0]]['density'] + $keywords[$keys[1]]['density']);
    }

    //Формируем шапку таблицы:
    $header = array(
      t('Site Name'),
      t('Length'),
      t('Keywords'),
      t('Total Density'),
    );

    $out .= theme_table($header, $rows);
    $keywords_found = TRUE;
  }


  //Выводим страницу:
  if (!$keywords_found) {
    why_keywords_not_found();
    return t('Keywords is not found.');
  }
  else    return $out;
}







function mb_ucfirst($str, $enc = NULL) {
  if ($enc === NULL) $enc = mb_internal_encoding();
  return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
}


/**
 * Find a reson why Keywords isn't found and give an advice to admin
 */

function why_keywords_not_found() {
  $result = db_fetch_array(db_query("SELECT COUNT(nid) AS count FROM {node}"));
  if ($result['count']>0) {
    //Причина - ноды есть, но вероятно не обработаны:
    drupal_set_message(t('<strong>Problem</strong>: keywords wasn\'t generated yet.<br /><strong>Reason</strong>: (1) Existing nodes has been created before Keywords module was installed or (2) nodes is not published or (3) node\'s content is too small to have keywords. <br /><strong>Solution</strong>: To generate keywords you can (1) !link1 or (2) visit node\'s tab called "Keywords" or (3) !link2 or (4) publish existing nodes or (5) extend node\'s content by adding more text to them.',
      array('!link1' => l('run cron manualy', 'admin/reports/status/run-cron'),
            '!link2' => l(t('create and save new node'), 'node/add')
      ) ), 'error');
  }
  else {
    drupal_set_message(t('<strong>Problem</strong>: keywords wasn\'t generated yet.<br /><strong>Reason</strong>: Nodes isn\'t found. <br /><strong>Solution</strong>: You should !link and keywords will be generated and listed here.', array('!link' => l(t('create and save new node'), 'node/add')) ), 'error');

  }
  //TODO: Ещё вариант - нет нод опред. типа - пока это не реализовано.
}



function count_nodes($status=1) {
  $sql = 'SELECT COUNT(nid) AS count FROM {node} WHERE status ='. (($status) ? '1' : '0');
  $result = db_fetch_array(db_query($sql));
  return $result['count'];
}

function count_parsed_nodes($status=1) {
  $sql = "SELECT ks.nid
          FROM {keywords_statistics} AS ks
            LEFT JOIN {node} AS n USING (nid)
          WHERE n.status=". (($status) ? '1' : '0') ."
            AND ks.param_id=1";
  $result = db_query($sql);
  $i=0;
  while (db_fetch_array($result))   $i++;
  return $i;
}

//AND kn.nid IS NOT NULL

//SELECT COUNT(kn.nid) AS count FROM keywords_nodes AS kn LEFT JOIN node AS n USING (nid) WHERE n.status=1 GROUP BY kn.nid



