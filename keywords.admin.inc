<?php
/**
 * @file keywords.admin.inc Hooks for settings page.
 */



/**
 * Implementation of hook_perm().
 */
function keywords_perm() {
  return array('administer keywords', 'view node keywords');
}


/**
 * Implementation of hook_menu()
 */
function keywords_menu() {
  $items['admin/settings/keywords'] = array(
    'title' => 'Keywords',
    'description' => 'Configure keywords.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('keywords_settings_form'),
    'access callback' => 'user_access',
    'access arguments' => array('administer keywords'),
  );
  $items['node/%node/keywords'] = array(
    'title' => 'Keywords',
    'page callback' => 'page_node_keywords',
    'page arguments' => array(1),
    'access callback' => '_keywords_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'keywords.pages.inc',
  );
  $items['node/%node/text_statistics'] = array(
    'title' => 'Text Statistics',
    'page callback' => 'page_node_text_statistics',
    'page arguments' => array(1),
    'access callback' => '_keywords_access',
    'access arguments' => array(1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 3,
    'file' => 'keywords.pages.inc',
  );

  $items['admin/content/keywords'] = array(
    'title' => 'All Keywords',
    'page callback' => 'page_all_keywords',
    'page arguments' => array(1),

    'access arguments' => array('administer keywords'),

    'file' => 'keywords.pages.inc',
  );
  $items['admin/content/keywords/all'] = array(
    'title' => 'All Keywords',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['admin/content/keywords/site_names'] = array(
    'title' => 'Site\'s Names',
    'description' => 'Site\'s keywords used for generating alternate site names.',
    'page callback' => 'page_site_names',
    'page arguments' => array(1),

    'access arguments' => array('administer keywords'),

    'parent' => 'admin/content/keywords',
    'file' => 'keywords.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['admin/content/keywords/node_statistics'] = array(
    'title' => 'Node Statistics',
    'description' => 'Node Statistics',
    'page callback' => 'page_node_statistics',
    'page arguments' => array(1),

    'access arguments' => array('administer keywords'),

    'parent' => 'admin/content/keywords',
    'file' => 'keywords.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  return $items;
}





/**
 * Menu item access callback - determine if the keywords tab is accessible.
 **/
function _keywords_access($node) {
  return user_access('view node keywords') && node_access('view', $node);
}





/**
 * Implementation hook_help()
 */
function keywords_help($path, $arg) {
  switch ($path) {
    case 'admin/settings/keywords':
          return t('Here you can configure the Keywords module. Keywords for each node is listed at node\'s tab named "Keywords". Keywords for all nodes is listed at !link page.',
          array('!link' => l( t('All Keywords'), 'admin/content/keywords') ) );
    case 'admin/help#keywords':
          return '<p>'. t('This module implements a simple filter to add the nofollow tag to sites that are on your blacklist or to all sites except those on your whitelist.') .'</p>
          <p>'. t('When keywords is saved to taxonomy only string with lenght 255 symbols is allowed. So we save keywords with highest density and less worrds in keywords is preffered.') .'</p>
          <p>'. t('At Cron runs we remove orphans keywords, orphans node statistics and generate keywords for nodes without keywords.') .'</p>';
    case 'node/%/keywords':
          return '<p>'. t('Tip: keywords are generates when node is saving.') .'</p>';


  }
}



/**
 * Builds the form for module settings
 */
function keywords_settings_form() {

  //Проверить изменения в настройках модуля и сохранить:
  include_once('keywords_settings_history.class.inc');
  $history = new Settings_History();
  if (!$history->is_current_settings_up_to_date())    $history->save_settings();

  $form['keywords_max_words'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of words in keywords phrases'),
    '#default_value' => variable_get('keywords_max_words', '2'),
    '#size' => 5,
    '#maxlength' => 3,
    '#description' => t('Recomended: 2. If text is large than bigger number of words can made you site much slower.'),
  );

  $form['keywords_min_density'] = array(
    '#type' => 'textfield',
    '#title' => t('Minimum number of times keywords used'),
    '#default_value' => variable_get('keywords_min_density', '2'),
    '#size' => 5,
    '#maxlength' => 3,
    '#description' => t('How many times keyword should be used in text to be saved as term in specified vocabulary? Recomended: 2 times.'),
  );



  if (function_exists('taxonomy_get_vocabularies')) {
    $select = array('0' => "Don't save keywords");
    foreach (taxonomy_get_vocabularies() as $vocabulary) {
      $select[$vocabulary->vid] = check_plain($vocabulary->name);
    }
    if (count($select) > 0) {
        $form['keywords_vid'] = array(
          '#type' => 'select',
          '#title' => t('Vocabulary for keywords'),
          '#default_value' => variable_get('keywords_vid', '0'),
          '#options' => $select,
          '#description' => t('Taxonomy vocabulary is used to store a node\'s keywords. Choose existing vocabulary or !link.', array('!link' => l('create new vocabulary', 'admin/content/taxonomy/add/vocabulary') )),
          '#multiple' => FALSE,
        );
    }
  }

 //Массив вида: a:2:{s:2:"ru";s:2:"ru";s:2:"en";s:2:"en";}
  $form['keywords_stop_words'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Stop-words'),
    '#default_value' => variable_get('keywords_stop_words', array('ru' => 'ru', 'en' => 'en')),
    '#multiple' => TRUE,
    '#options' => array(
      'ru' => t('Russian stop-words'),
      'en' => t('English stop-words'),
    ),
    '#description' => t('Should stop-words be removed from keywords list? Choose language of stop-words for removing.'),
  );


  $form['keywords_cron_max_nodes'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum number of nodes parsed for keywords at every cron run'),
    '#default_value' => variable_get('keywords_cron_max_nodes', '1'),
    '#size' => 5,
    '#maxlength' => 2,
    '#description' => t('Default is 1. Please remember that generating keywords can take a lot of time for node\'s that has a big size. Better solution is run cron more often. Using "0" will disable keywords generation at cron run.'),
  );




 //Массив вида: a:2:{s:5:"Title";s:5:"Title";s:6:"Teaser";s:6:"Teaser";}
  $form['keywords_area'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Keywords Area'),
    '#default_value' => variable_get('keywords_area', array('title' => 'title', 'teaser' => 'teaser')),
    '#multiple' => TRUE,
    '#options' => array(
      'title' => t('Title'),
      'teaser' => t('Teaser'),
//      'tags' => t('tags'),
//      'boxes' => t('Boxes'),
    ),
    '#description' => t('Keyword are looking at node\'s body but you can choose additional areas where keywords will be looking for.'),
  );



  return system_settings_form($form);
}





/**
 * Implementation of hook_cron().
 *
 * Удаляем ключевые слова, которые не принадлежат нодам
 * Берем 1 ноду, у которой нет ключевых слов и парсим её текст.
 * @return
 * nothing
 */
function keywords_cron() {
  //Удалить из таблицы keywords_text ключевые слова, которых нет ни в одной ноде
  db_query("DELETE kt.*
            FROM {keywords} kt
              LEFT JOIN {keywords_nodes} kn USING (kwid)
            WHERE kn.kwid IS NULL");
  watchdog('keywords', 'Was removed orphan keywords at cron run.');

  //Сколько нод обрабатывать за 1 раз:
  $max_nodes = variable_get('keywords_cron_max_nodes', '1');
  if ($max_nodes) {
    set_time_limit(0); //Не обязательно
    // Получить id нод, которые не имеют ключевых слов:
    $cron_nodes = get_node_without_keywords($max_nodes);
    foreach ($cron_nodes as $nid) {
      if ($nid===FALSE) {
        watchdog('keywords', 'Nothing was done at cron run - all nodes has keywords.');
      }
      else {
        generate_keywords_for_node($nid);
        watchdog('keywords', 'Keywords for node # !nid was generated at cron run.', array('!nid' => $nid), WATCHDOG_INFO, l(t('View Keywords'), 'node/'. $nid .'/keywords'));
      }
    }
  }
}

/**
 * Получить nid ноды, у которой нет списка ключевых слов
 *
 * Получаем 1 опубликованную ноду, у которой нет количества слов в тексте.
 * Сначала берутся статьи, которые давно не менялись - старые статьи, потому что они
 * наверняка проиндексированы поисковиками и их нужно учесть в первую очередь.
 *
 * @param $n
 * integer  Сколько нужно нод без ключевых слов
 * @return
 * array    nid нод, у которых нет ключевых слов
 */
function get_node_without_keywords($n=1) {
  $nodes =  array();
  $sql = "SELECT n.nid
          FROM {node} as n
            LEFT JOIN {keywords_nodes} kn USING (nid)
          WHERE kn.nid IS NULL
            AND n.status=1
          GROUP BY n.nid
          ORDER BY n.changed ASC, n.created ASC
          LIMIT %d";
  $result = db_query($sql, $n);
  while ($nid = db_fetch_array($result)) {
    $nodes[] = $nid['nid'];
  }
  return $nodes;
}

