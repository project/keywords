<?php
/**
 * @file keywords_statistics.class.inc  class Keywords_Statistics.
 * @class Keywords_Statistics  Class that handle node's statistics.
 *
 *
 * Usage:
 *   include_once('keywords_statistics.class.inc');
 *   $statistics = new Keywords_Statistics();
 *   $statistics->get_description('original_body_length');
 */

Class Keywords_Statistics {
  var $param; //Данные о параметрах статистики
  var $param_name; //Названия параметров

  function keywords_statistics() {
    $this->param = array(
      'original_body_length' => '1',
      'cleared_body_length' => '2',
      'original_teaser_length' => '3',
      'cleared_teaser_length' => '4',
      'original_title_length' => '5',
      'cleared_title_length' => '6',
      'body_words_with_stopwords' => '7',
      'body_words_without_stopwords' => '8',

      'count_words_full_text_without_stopwords' => '50',
    );


    $this->param_name = array(
      '1' => t('Length of original node\'s body text, symbols'),
      '2' => t('Length of cleared node\'s body text, symbols'),
      '3' => t('Length of original node\'s teaser text, symbols'),
      '4' => t('Length of cleared node\'s teaser text, symbols'),
      '5' => t('Length of original node\'s title text, symbols'),
      '6' => t('Length of cleared node\'s title text, symbols'),
      '7' => t('Length of original node\'s teaser text, symbols'),
      '8' => t('Length of cleared node\'s teaser text, symbols'),

      '50' => t('Number of words in full text without stop-words, words'),
    );

  }


  /**
  * Значение произвольного параметра
  */
  function get_all($nid='') {
    if (!$nid)  return;
    $sql = "SELECT param_id, value FROM {keywords_statistics} WHERE nid='%d'";
    $result = db_query($sql, $nid);
    while ($r = db_fetch_array($result)) {
      $p[] = array($this->param_name[$r['param_id']], $r['value']);
    }
    return $p;
  }



  /**
  * Значение произвольного параметра
  */
  function get_param_value($nid='', $code='') {
    if (!$nid || !$code)  return;
    $sql = "SELECT value FROM {keywords_statistics} WHERE nid='%d' AND param_id=%d";
    $result = db_fetch_array(db_query($sql, $nid, $this->get_id($code)));
    return $result['value'];
  }




  /**
  * Общее количество слов ноды
  */
  function get_node_total_words($nid='') {
    return $this->get_param_value($nid, 'count_words_full_text_without_stopwords');
  }



  function get_id($code='') {
    if ($code)  return $this->param[$code];
  }

  function get_description($code='') {
    if ($code)  return $this->param_name[$code];
  }


  /**
  * Удалить параметер из статистики ноды
  *
  * @param $nid
  * integer   Node's ID
  * @param $pid
  * integer   Parameter ID. If not set all node's statistics will be removed.
  * @return
  * nothing
  **/
  function delete($nid='', $pid='') {
    if (!$nid)   return FALSE;
    if (!$pid) {
      db_query("DELETE FROM {keywords_statistics} WHERE nid = %d", $nid); //Удалить всю статистику
    }
    else {
       db_query_range("DELETE FROM {keywords_statistics} WHERE nid = %d AND param_id = %d", $nid, $pid, 0, 1);
    }
  }


  /**
  * Сохранить параметер в статистике ноды
  *
  * @param $nid
  * integer   Node's ID
  * @param $pid
  * integer   Parameter ID.
  * @param $value
  * integer   Parameter value.
  * @return
  * nothing
  */
  function save($nid='', $code='', $value='') {
    if (!$nid || !$code)   return FALSE;
    $sql ="INSERT INTO {keywords_statistics} (`nid`, `param_id`, `value`) VALUES ('%d', '%d', '%s')";
    db_query($sql, $nid, $this->get_id($code), $value);
  }

} //Class end